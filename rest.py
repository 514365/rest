from flask import Flask, request, jsonify
import requests

app = Flask(__name__)

def get_airport_temp(query):
    res = requests.get("https://www.airport-data.com/api/ap_info.json?iata=" + query)
    airport_code = res.json()['iata']
    temp = requests.get("http://api.weatherapi.com/v1/current.json?key=2a20703d6c0d4d28903100448242904&q=iata:" + airport_code)
    return temp.json()['current']['temp_c']

def get_stock_price(query):
    stock_info = requests.get("https://api.tiingo.com/tiingo/daily/" + query + "/prices?token=3d32dedcd2e7542f6e45fb2bdab21e99e7d86143")
    return stock_info.json()[0]['adjClose']

def evaluate_expression(query):
    query = query.replace(" ", "+")
    return eval(query)

@app.route('/api', methods=['GET'])
def api():
    query_funcs = {
        'queryAirportTemp': get_airport_temp,
        'queryStockPrice': get_stock_price,
        'queryEval': evaluate_expression
    }

    query_param = None
    for param in query_funcs.keys():
        query_param = request.args.get(param)
        if query_param:
            result = query_funcs[param](query_param)
            return jsonify(result)

    return jsonify(None), 400

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9090, debug=True)