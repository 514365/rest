from flask import Flask, request, jsonify
import requests

app = Flask(__name__)

@app.route('/api', methods=['GET'])
def api():
    queryAirportTemp = request.args.get('queryAirportTemp')
    queryStockPrice = request.args.get('queryStockPrice')
    queryEval = request.args.get('queryEval', type=str)

    result = None

    if queryAirportTemp:        
        print(queryAirportTemp)
        res = requests.get("https://www.airport-data.com/api/ap_info.json?iata=" + queryAirportTemp)
        print('Airport code: ', res.json()['iata'])
        temp = requests.get("http://api.weatherapi.com/v1/current.json?key=3ba3ac0e6ea84839b9e85951242704&q=iata:" + res.json()['iata'])
        print('Airport temp: ', temp.json()['current']['temp_c'])
        # Call the API to get the temperature for the given airport
        # You'll need to fill in the details here
        result = temp.json()['current']['temp_c']
        pass
    elif queryStockPrice:
        print(queryStockPrice)
        stockInfo = requests.get("https://api.tiingo.com/tiingo/daily/" + queryStockPrice + "/prices?token=ce706e324a0912c4a722f6338987aa71f5db4ae3")
        print(stockInfo.json()[0]['adjClose'])
        result=stockInfo.json()[0]['adjClose']
        # Call the API to get the stock price for the given stock
        # You'll need to fill in the details here
        pass
    elif queryEval:
        # Evaluate the arithmetic expression
        # You'll need to fill in the details here
        queryEval = queryEval.replace(" ", "+")
        result=eval(queryEval)
        pass
    else: 
        return jsonify(None), 400

    return jsonify(result)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8250, debug=True)

